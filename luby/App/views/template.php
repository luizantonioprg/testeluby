<!DOCTYPE html>
<html>
<head>
	<title>Template Luby</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta charset="UTF-8">	
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" type="text/css">
    

	<style>
		<?php include 'css/style.css'; ?>
	</style>

</head>
<body class="bg-light">
	

	


	<header>
		<h1 class="bg-danger p-5 text-white">LUBY</h1>
	<header>


<!--
	<a class="btn btn-danger m-2" href="/">Home</a>
-->

	<!--

	<?php if(!isset($_SESSION['logado'])): ?>
		<a class="btn btn-danger m-2" href="/home/login">Login</a>
		

	<?php endif; ?>

	-->

	<?php if(isset($_SESSION['logado'])): ?>
		<a class="btn btn-danger m-2" href="/usuarios/cadastrar">Cadastrar Aluno</a>
		<a class="btn btn-danger m-2" href="/home/logout">Logout</a>
	<?php endif; ?>



	<div class="content">
		<?php
	            require_once '../App/views/'.$view.'.php';
		?>
	</div>

	<footer class="bg-danger p-2">
		<h4 class="text-white">Apenas um footer</h4>

	</footer>

</body>
</html>