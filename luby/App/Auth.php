<?php
namespace App;
use App\Core\Db_connection;
class Auth{

	public static function Login($email,$senha){

        $sql = "SELECT * FROM usuarios WHERE login=?";
        $stmt = Db_connection::getConn()->prepare($sql);
        $stmt->bindValue(1,$email);

      

        $stmt->execute();

        if($stmt->rowCount() >= 1)://achou email 
                //return 'encontrou';
                $resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
                if(password_verify($senha,$resultado['senha'])):
                     $_SESSION['logado'] = true;
                     header('Location:/home/index');
                 else:
                    return "senha invalida";
                 endif;
               
        else:
                return "erro";

        endif;

		
	}

	public static function Logout(){

		session_destroy();
		header('Location:/home/login');
	}

     public static function checkLogin(){

                if(!isset($_SESSION['logado'])):
                  header('Location:/home/login');
                endif;
     }

}