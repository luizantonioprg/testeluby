<?php

use App\Core\Db_connection;

class Usuario extends Db_connection{

	public $nome;
    public $idade;
    public $sexo;
    public $login;
    public $senha;
    public $ano_escolar;
    public $nivel_privilegio;

	public function index(){
		echo "oi";
	}

	    public function create(){
        $sql = "INSERT INTO `usuarios`(`nome`, `idade`, `sexo`, `ano_escolar`, `nivel_privilegio`, `senha`, `login`) VALUES (?,?,?,?,?,?,?);";
        $stmt = Db_connection::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->nome);
        $stmt->bindValue(2,$this->idade);
        $stmt->bindValue(3,$this->sexo);
        $stmt->bindValue(4,$this->ano_escolar);
        $stmt->bindValue(5,$this->nivel_privilegio);
        $stmt->bindValue(6,$this->senha);
        $stmt->bindValue(7,$this->login);

        if($stmt->execute()){
            return "Cadastrado com sucesso!";

        }else{
            return "Erro ao cadastrar";

        }

    }

        public function getAll(){
            $sql = "SELECT * FROM `usuarios`;";
            $stmt = Db_connection::getConn()->prepare($sql);
            $stmt->execute();

            if($stmt->rowCount() > 0):

                $resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                return $resultado;
            else:
                return [];
            endif;


        }

    public function delete($id){

        $sql = "DELETE FROM usuarios WHERE id=?;";
        $stmt = Db_connection::getConn()->prepare($sql);
        $stmt->bindValue(1,$id);


        if($stmt->execute()):

            return "Excluido com sucesso";
        else:
                return "Erro ao excluir";
        endif;

    }

    public function update($id){


        $sql = "UPDATE `usuarios` SET `nome`= ?,`idade`= ? WHERE id = ?;";
        $stmt = Db_connection::getConn()->prepare($sql);
        $stmt->bindValue(1,$this->nome);
        $stmt->bindValue(2,$this->idade);
        $stmt->bindValue(3,$id);


        if($stmt->execute()){
            return "Atualizado com sucesso!";

        }else{
            return "Erro ao atualizar";

        }
    }

}