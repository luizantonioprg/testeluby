<?php
use \App\Core\ControllerCore;
use App\Auth;

class Home extends ControllerCore{

    public function index(){
        Auth::checkLogin();
        $user = $this->model('Usuario');
        $dados = $user->getAll();
        
        
        $this->view('home/index',$dados=['registros'=>$dados]);

     }

     public function login(){

     	$dados = array();


     	if(isset($_POST['entrar'])):
     		 if( (empty($_POST['email'])) or (empty($_POST['senha']))):
     			$dados[]= "O campo email e senha são obrigatórios";

     		else:

     		$email = $_POST['email'];
     		$senha = $_POST['senha'];

     		$dados[] = Auth::Login($email,$senha);

        	 endif;
		endif;

     	$this->view('home/login',$dados=['mensagem'=>$dados]);
     }


        public function logout(){
            Auth::Logout();

        }

    }