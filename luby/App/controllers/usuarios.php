<?php
use \App\Core\ControllerCore;
use App\Auth;



class Usuarios extends ControllerCore{

    public function index($nome=''){
        $note = $this->model('Usuario');
        $dados = [];
        
        
        $this->view('home/index',$dados = ['registros' => $dados]);
    }



    public function cadastrar(){
        Auth::checkLogin();
        $mensagem = array();
        

  
        if(isset($_POST['cadastrar'])):
            $nome = $_POST['nome'];
            $idade = $_POST['idade'];
            $sexo = $_POST['sexo'];
            $login = $_POST['login'];
            $senha = password_hash($_POST['senha'],PASSWORD_DEFAULT);
            $ano_escolar = $_POST['ano_escolar'];
            $nivel_privilegio = $_POST['nivel_privilegio'];


            $user = $this->model('Usuario');
            $user->nome = $nome;
            $user->idade = $idade;
            $user->sexo = $sexo;
            $user->login = $login;
            $user->senha = $senha;
            $user->ano_escolar = $ano_escolar;
            $user->nivel_privilegio = $nivel_privilegio;

            $mensagem[] = $user->create();


        endif;

        $this->view('Admin/cadastrar',$dados = ['mensagem' => $mensagem]);

    }


    public function exibirAlunos($id=''){

        $aluno = $this->model('Usuario');
        $dados = $aluno->getAll();
        
        
        $this->view('home/index',$dados=['registros'=>$dados]);


    }

    public function excluir($id=''){
         Auth::checkLogin();
        $mensagem = array();
        $aluno = $this->model('Usuario');
        $mensagem[] = $aluno->delete($id);
        $dados = $aluno->getAll();

    $this->view('home/index',$dados = ['registros'=>$dados,'mensagem'=>$mensagem]);

    }

    public function editar($id){
         Auth::checkLogin();
        $mensagem = array();
        $aluno = $this->model('Usuario');
        if(isset($_POST['atualizar'])):

            $aluno->nome = $_POST['nome'];
            $aluno->idade = $_POST['idade'];
            $id = $_POST['id'];

           $mensagem[] = $aluno->update($id);
           

        endif;

    

        $this->view('Admin/editar',$dados = ['mensagem' => $mensagem]);
    }





    
}